TEMPLATE = app

TARGET = thumbnailer-demo
DEPENDPATH += .

CONFIG += qt \
          qdbus

LIBS += -lduithumbnailer -ldui


HEADERS += simple.h
SOURCES += simple.cpp

target.path =    $$PREFIX/usr/bin
sources.files = $$SOURCES $$HEADERS simple.pro
resources.path = $$PREFIX/usr/share/thumbexamples/data
resources.files = ./data/*
INSTALLS += target 
INSTALLS += resources
