#include <QObject>
#include <QString>
#include <QUrl>
#include <QDebug>
#include <QDir>

#include <QMainWindow>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QProgressBar>
#include <QCheckBox>

#include <duithumbnailer/Thumbnailer>
#include <dui/DuiApplicationWindow>
#include <dui/DuiApplication>

class ApplicationWindow: public QObject{//: public DuiApplicationWindow {
Q_OBJECT
private:
	QLabel*      description;
	QLineEdit*   directory;
	QPushButton* action;
	QPushButton* stopAction;
	QVBoxLayout* vbox;
	QHBoxLayout* hbox;
	QGridLayout* thumbs;
	QProgressBar* progress;
	QCheckBox*   sendRemaining;

	QString path;
	QHash<QString, QLabel*> widgets;

	Thumbnailer* thumb;

	QPixmap dequeuedPixmap;
	QPixmap errorPixmap;
public:
	ApplicationWindow(char* path);
	~ApplicationWindow();

public Q_SLOTS:
	void loadThumbnails();
	void cancelRequest();
	void loadingStarted();
	void loadingFinished(int left);

	void thumbnail(QUrl uri, QUrl thumb, QPixmap pixmap);
	void defaultThumbnail(QUrl uri, QUrl thumb, QPixmap pixmap);
	void dequeued (QUrl uri);
	void error (QString error, QUrl uri);
signals:
    void isFinished();
};



