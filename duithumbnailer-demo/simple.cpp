#include "simple.h"
#include <QScrollArea>
#include <QFrame>
#include <QMultiHash>
//#include <QCoreApplication>
#include <QApplication>
int
main(int argc, char* argv[]) {
	qDebug() << "Simple thumbnailing example application";
    //DuiApplication application( argc, argv );
    //QCoreApplication application(argc, argv);
    QApplication application(argc, argv);
    ApplicationWindow mw(argv[1]);
    QObject::connect(&mw, SIGNAL(isFinished()), &application, SLOT(quit()));
    return application.exec();

}


ApplicationWindow::~ApplicationWindow() {
   	if(NULL != thumb) {
		thumb->cancel(false);
		delete thumb;
	}
}

ApplicationWindow::ApplicationWindow(char* path)// : DuiApplicationWindow()
{
	this->path = QString(path);
	if(this->path.isEmpty()) this->path = QString("/home/user/MyDocs/.images");
    thumb = NULL;
    loadThumbnails();

}

void ApplicationWindow::cancelRequest() {
	if(NULL == thumb) return;
	qDebug() << "Canceling all requests...";

	bool send = sendRemaining->isChecked();
	thumb->cancel(send);
}

void ApplicationWindow::loadThumbnails() {
	QDir dir(path);
	QStringList filters; filters << "*.jpg" << "*.jpeg" << "*.JPG" << "*.JPEG" << "*.avi" <<"*.mpg" <<"*.ogg"<<"*.mp4"<<"*.txt";
	dir.setNameFilters(filters);
	QStringList files = dir.entryList(QDir::Files | QDir::Readable);
	QList<QUrl> uris;
	QStringList mimes;

	foreach(QString file, files) {
		file = "file://" + path + "/" + file;
		uris << QUrl(file);
		if(file.endsWith(".avi"))
            mimes << "video/x-msvideo";
        else if(file.endsWith(".mpg"))
            mimes << "video/mpeg";
        else if(file.endsWith(".ogg"))
            mimes << "video/x-theora+ogg";
        else if(file.endsWith(".mp4"))
            mimes << "video/mp4";
        else if(file.endsWith(".txt"))
            mimes << "text/plain";
		else
            mimes << "image/jpeg";
	}

	if(NULL == thumb) {

        QMultiHash<QString,QString> hashTable = Dui::Thumbnailer::getSupported();
        QList<QString> keyList = hashTable.keys();
        QList<QString> valueList = hashTable.values();
        for(int i=0; i<keyList.count();i++){
            qDebug()<<"The key is: "<<keyList.at(i)<<" the value is: "<<valueList.at(i);
        }
        
        thumb = new Thumbnailer();
        qDebug()<<"+++ApplicationWindow::loadThumbnails():it goes here 7";
		QObject::connect(thumb, SIGNAL(started()), this, SLOT(loadingStarted()));
		QObject::connect(thumb, SIGNAL(finished(int)), this, SLOT(loadingFinished(int)));
		QObject::connect(thumb, SIGNAL(thumbnail(QUrl,QUrl,QPixmap,QString)), this, SLOT(thumbnail(QUrl,QUrl,QPixmap)));
		QObject::connect(thumb, SIGNAL(defaultThumbnail(QUrl,QUrl,QPixmap,QString)), this, SLOT(defaultThumbnail(QUrl,QUrl,QPixmap)));
		QObject::connect(thumb, SIGNAL(error(QString,QUrl)), this, SLOT(error(QString,QUrl)));
		QObject::connect(thumb, SIGNAL(dequeued(QUrl)), this, SLOT(dequeued(QUrl)));
	}

	bool done = false;
	done = thumb->request (uris, mimes, true); // true - send also a pixmap with the thumbnail
    if(done) {
		qDebug() << "All thumbnails were ready at the request time - no need for dbus traffic!";
	}
}

void ApplicationWindow::loadingStarted() {
}

void ApplicationWindow::loadingFinished(int left) {
    if(left == 0)
        emit isFinished();
}

void ApplicationWindow::thumbnail(QUrl uri, QUrl thumb, QPixmap pixmap) {
	Q_UNUSED(thumb);
}

void ApplicationWindow::defaultThumbnail(QUrl uri, QUrl thumb, QPixmap pixmap) {
	Q_UNUSED(thumb);
}

void ApplicationWindow::dequeued (QUrl uri) {

}

void ApplicationWindow::error (QString error, QUrl uri) {
    qDebug()<<"+++ApplicationWindow::error():There is an error: "<<error<<" the uri is:"<<uri.toString();
}
