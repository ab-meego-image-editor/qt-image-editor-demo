TEMPLATE = app
TARGET = mdtest
DEPENDPATH += .
INCLUDEPATH += $$[QT_INSTALL_HEADERS]/quillmetadata/ .

# Input
SOURCES += metadatatest.cpp
PKGCONFIG += quillmetadata
LIBS += -lquillmetadata
