// Metadata test tool with Libexif

#include <QDebug>
#include <QVariant>
#include <QStringList>
#include <QuillMetadata>
#include <QFile>

int main(int argc, const char** argv)
{
    qDebug() << "--- Quill metadata reader ---";

    if (argc == 1) {
        qDebug() << "To view image metadata          : metadatatest [filename]";
        qDebug() << "To vrite image metadata         : metadatatest [infilename] [outfilename]";
        qDebug() << "To clear image metadata         : metadatatest [filename] [outfilename] [tag]";
        qDebug() << "To modify image metadata        : metadatatest [filename] [outfilename] [tag] [value] [value...]";
	qDebug() << "Tags that support modify/clear  : Creator, Subject, City, Country, Rating, Description, Title,";
	qDebug() << "                                  Orientation, GPS_lat, GPS_lon, GPS_alt";
	qDebug() << "Tags that support clear	     : GPS (clears all GPS related data)";
        return 0;
    }

    QString fileName = argv[1];

    QuillMetadata metadata = QuillMetadata(fileName);

    if (QString(argv[3]) == QString("GPS")) {
        qDebug() << "Clearing GPS metadata";
        metadata.removeEntries(QuillMetadata::TagGroup_GPS);
    }
    else{
        for(int i=3; i<argc;i++,i++){
            int content = i+1;
            if (QString(argv[i]) == QString("Creator"))
                metadata.setEntry(QuillMetadata::Tag_Creator, QString(argv[content]));
            if (QString(argv[i]) == QString("City"))
                metadata.setEntry(QuillMetadata::Tag_City, QString(argv[content]));
            if (QString(argv[i]) == QString("Country"))
                metadata.setEntry(QuillMetadata::Tag_Country, QString(argv[content]));
            if (QString(argv[i]) == QString("Rating"))
                metadata.setEntry(QuillMetadata::Tag_Rating, QString(argv[content]));
            if (QString(argv[i]) == QString("Description"))
                metadata.setEntry(QuillMetadata::Tag_Description, QString(argv[content]));
            if (QString(argv[i]) == QString("Orientation"))
                metadata.setEntry(QuillMetadata::Tag_Orientation, QString(argv[content]));
            if (QString(argv[i]) == QString("Location"))
                metadata.setEntry(QuillMetadata::Tag_Location, QString(argv[content]));
	    if (QString(argv[i]) == QString("GPS_lat"))
		metadata.setEntry(QuillMetadata::Tag_GPSLatitude, QString(argv[content]));
	    if (QString(argv[i]) == QString("GPS_lon"))
		metadata.setEntry(QuillMetadata::Tag_GPSLongitude, QString(argv[content]));
	    if (QString(argv[i]) == QString("GPS_alt"))
		metadata.setEntry(QuillMetadata::Tag_GPSAltitude, QString(argv[content]));
	    if (QString(argv[i]) == QString("Subject")) {
                QStringList list;
                for (int j=content; j<argc; j++)
		    list << argv[j];
		qDebug() << "Setting value of" << QString(argv[i]) << "to";
		for (int j=content; j<list.length(); j++)
		    qDebug() << list[j];
		metadata.setEntry(QuillMetadata::Tag_Subject, list);
	    } else {
		qDebug() << "Setting value of" << QString(argv[i]) << "to" << QString(argv[content]);
	    }
        }
    }

    if (argc > 2) {
        QString targetFileName = argv[2];

        qDebug() << "-- Writing metadata --";

	FILE *targetfile = fopen(targetFileName.toAscii().constData(), "r");
	if (!targetfile) {
	    qDebug() << "Creating file" << targetFileName;
	    QFile::copy(fileName, targetFileName);
	} else
	    fclose(targetfile);

	qDebug() << "Writing to file" << targetFileName;
	if (!metadata.write(targetFileName))
            qDebug() << "-- Write failed! --";

	qDebug() << "Selected metadata for" << targetFileName;
    }
    else
	qDebug() << "Selected metadata for" << fileName;

    qDebug() << "-- EXIF metadata --";
    qDebug() << "Camera make                  : " << metadata.entry(QuillMetadata::Tag_Make);
    qDebug() << "Camera model                 : " << metadata.entry(QuillMetadata::Tag_Model);
    qDebug() << "Focal length                 : " << metadata.entry(QuillMetadata::Tag_FocalLength);
    qDebug() << "Exposure time                : " << metadata.entry(QuillMetadata::Tag_ExposureTime);
    qDebug() << "Originally created           : " << metadata.entry(QuillMetadata::Tag_TimestampOriginal);
    qDebug() << "Orientation                  : " << metadata.entry(QuillMetadata::Tag_Orientation);
    qDebug() << "GPS latitude                 : " << metadata.entry(QuillMetadata::Tag_GPSLatitude);
    qDebug() << "GPS longitude                : " << metadata.entry(QuillMetadata::Tag_GPSLongitude);
    qDebug() << "GPS altitude                 : " << metadata.entry(QuillMetadata::Tag_GPSAltitude);
    qDebug() << "-- XMP and IPTC IIM metadata --";
    qDebug() << "[DC] Creator                 : " << metadata.entry(QuillMetadata::Tag_Creator);
    qDebug() << "[DC] Subject                 : " << metadata.entry(QuillMetadata::Tag_Subject);
    qDebug() << "[Photoshop/IPTC IIM] City    : " << metadata.entry(QuillMetadata::Tag_City);
    qDebug() << "[Photoshop/IPTC IIM] Country : " << metadata.entry(QuillMetadata::Tag_Country);
    qDebug() << "[XMP] Rating                 : " << metadata.entry(QuillMetadata::Tag_Rating);
    qDebug() << "[DC] Description             : " << metadata.entry(QuillMetadata::Tag_Description);
    qDebug() << "[DC] Title                   : " << metadata.entry(QuillMetadata::Tag_Title);
}
