#ifndef DBUS_SERVICES_P_H_
#define DBUS_SERVICES_P_H_

#include <QObject>
#include <QHash>
#include "dbus/thumbnailer_generic.h"
#include "dbus/tumbler_cache.h"

class DBusServices : public QObject {
Q_OBJECT
	private:
		DBusServices();
		~DBusServices();
		static void reconnectTumblerService();
		static void reconnectMediaArtService();
	public:
    static ThumbnailerGenericCacheProxy* cache();
		static ThumbnailerGenericProxy*      tumbler();
    //static ComNokiaAlbumArtRequester*    mediaart();
		static DBusServices*                 instance();

	public Q_SLOTS:
		// just re-emit signals (but now the emmiter is always the same)
		// TUMBLER SERVICE
		inline void tumblerStartedHandler ( uint handle )
			{ emit StartedHandler(handle); }
		inline void tumblerReadyHandler   ( uint handle, const QStringList &uris )
			{ emit ReadyHandler(handle,uris); }
		inline void tumblerErrorHandler   ( uint handle,
		                                    const QStringList &failedUris,
		                                    int errorCode,
		                                    const QString &message )
			{ emit ErrorHandler(handle,failedUris,errorCode,message); }
		inline void tumblerFinishedHandler( uint handle )
			{ emit FinishedHandler(handle); }

Q_SIGNALS:
		// signals from TUMBLER
		void StartedHandler ( uint handle );
		void ReadyHandler   ( uint handle, const QStringList &uris );
		void ErrorHandler   ( uint handle,
		                      const QStringList &failedUris,
		                      int errorCode,
		                      const QString &message );
		void FinishedHandler( uint handle );

		// own signals
		void tumblerServiceReconnected();
		void cacheServiceReconnected();
		void mediaartServiceReconnected();
};

#endif
