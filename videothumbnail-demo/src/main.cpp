#include <QCoreApplication>
#include <QDebug>
#include <QDBusReply>
#include <QMultiHash>
#include <QStringList>
#include "dbus/thumbnailer_generic.h"
#include "dbus-services.h"
#include "videothumbnailer.h"


int
main(int argc, char* argv[]) {
	qDebug() << "Simple thumbnailing example application";
    //DuiApplication application( argc, argv );
    QCoreApplication application(argc, argv);

    VideoThumbnailer *thumbnailer = new VideoThumbnailer();
    //we connect the signals
    QObject::connect(thumbnailer, SIGNAL(finishGenerating()),&application,SLOT(quit()));
    QObject::connect(thumbnailer, SIGNAL(error()),&application,SLOT(quit()));

    QStringList uris;
    QStringList mime;
    QString flavor = QString("normal");
    if (argc >= 2)
        flavor = QString(argv[2]);
    QString scheduler = QString("foreground");
    QString fileName = QString(argv[1]);
    uris<<"file://"+QUrl(fileName).toString();
    if(fileName.endsWith(".mp4"))
        mime<< "video/mp4";
    else
        mime<<"image/jpeg";

    if(!thumbnailer->checkExist(fileName,flavor)){
        thumbnailer->makeRequest(uris,mime,flavor,scheduler,0);
    }
    return application.exec();
}
