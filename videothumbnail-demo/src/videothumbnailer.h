#ifndef VIDEO_THUMBNAILER_H_
#define VIDEO_THUMBNAILER_H_

#include <QObject>
#include <QDBusPendingCallWatcher>

class VideoThumbnailer:public QObject{
Q_OBJECT

public:
    VideoThumbnailer();
    void makeRequest(QStringList &uris,
                     QStringList &mime,
                     QString &flavor,
                     QString &scheduler,
                     uint handle);
    bool checkExist(QString &fileName, QString &flavor);

    void requestCallFinished(QDBusPendingCallWatcher* watcher);
    ~VideoThumbnailer();
public Q_SLOTS:
    
	void StartedHandler (uint handle);
	void ReadyHandler   (uint handle, const QStringList &uris);
	void ErrorHandler   ( uint handle,
	                      const QStringList &failedUris,
	                      int errorCode,
	                      const QString &message );
	void FinishedHandler(uint handle);

Q_SIGNALS:
    void finishGenerating();
    void error();
};

#endif 





