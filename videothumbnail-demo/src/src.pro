TEMPLATE = app
CONFIG += dll 
TARGET = videothumbnailer
DEPENDPATH += .
INCLUDEPATH += .
#include(base.pri)
#DEFINES += ENABLE_TIMER_DEBUG_OUTPUT

QT += dbus

# old stuff. Remove?
# have the lib dirs explicitly because qmake includes the Qt linking stuff
# before LIBS, which can cause problems with linking directory order
# QMAKE_LIBDIR += $$system(icu-config --icudata-install-dir)

# Input
HEADERS += dbus/dbus_types.h \
           dbus/thumbnailer_generic.h \
           dbus/tumbler_cache.h \
           dbus-services.h \
           videothumbnailer.h \

SOURCES += main.cpp \
           dbus/dbus_types.cpp \
           thumbnailer_generic.cpp \
           tumbler_cache.cpp \
           dbus-services.cpp \
           videothumbnailer.cpp \


message(Install prefix is $$install_prefix)

INSTALL_HEADERS = include/*
install_headers.path = $$install_prefix/include/duithumbnailer
install_headers.files = $$INSTALL_HEADERS

target.path += $$install_prefix/lib
target.depends += generateproxy generatecacheproxy generatemediaartproxy

INSTALLS += target \
    install_headers

clean.depends += cleanproxies
