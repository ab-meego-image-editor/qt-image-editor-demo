
#include "dbus-services.h"


/*            ------- STATIC DBUS SERVICES OBJECTS ------------             */

static QThreadStorage<ThumbnailerGenericProxy*>      _tumblerService;
static QThreadStorage<ThumbnailerGenericCacheProxy*> _cacheService;
/*
  static QThreadStorage<ComNokiaAlbumArtRequester*>    _mediaartService;
*/

DBusServices::DBusServices() : QObject()
{
	// we are not connecting to DBus at the initialization time
	// connection to DBus will tak place while trying access any service
}

DBusServices::~DBusServices()
{
	_tumblerService.setLocalData(0);
    _cacheService.setLocalData(0);
    /*
      _mediaartService.setLocalData(0);
    */
}

DBusServices*
DBusServices::instance()
{
	static DBusServices singleton;
	return &singleton;
}


/*            ------- STATIC DBUS SERVICES OBJECTS ------------             */
/*            -------       IMPLEMENTATION         ------------             */
static const char* _thumbnailer_service_g = "org.freedesktop.thumbnails.Thumbnailer1";
static const char* _thumbnailer_path_g    = "/org/freedesktop/thumbnails/Thumbnailer1";
static const char* _cache_service_g       = "org.freedesktop.thumbnails.Cache1";
static const char* _cache_path_g          = "/org/freedesktop/thumbnails/Cache1";
ThumbnailerGenericCacheProxy*
DBusServices::cache()
{
	/* check if there is no cache service yet or connection is lost */
	if ( !_cacheService.hasLocalData() ||
	     !_cacheService.localData()->isValid() )
	{
		_cacheService.setLocalData(
			new ThumbnailerGenericCacheProxy(
				QString(_cache_service_g),
				QString(_cache_path_g),
				QDBusConnection::sessionBus()
			)
		);

		// send signal about (re)conneecting to the dbus service
		emit instance()->cacheServiceReconnected ();
	}

	return _cacheService.localData();
}

ThumbnailerGenericProxy*
DBusServices::tumbler()
{
	/* check if there is no tumbler service yet or connection is lost */
	if ( !_tumblerService.hasLocalData() ||
	     !_tumblerService.localData()->isValid() )
	{
		_tumblerService.setLocalData(
			new ThumbnailerGenericProxy(
				QString(_thumbnailer_service_g),
				QString(_thumbnailer_path_g),
				QDBusConnection::sessionBus()
			)
		);
		// need to reconnect all signals and slots
		reconnectTumblerService ();
	}

	return _tumblerService.localData();
}

void
DBusServices::reconnectTumblerService()
{
	QObject::connect( _tumblerService.localData(),
	                  SIGNAL(Ready(uint,const QStringList&)),
	                  instance(),
	                  SLOT(tumblerReadyHandler(uint, const QStringList&)) );
	QObject::connect( _tumblerService.localData(),
	                  SIGNAL(Finished(uint)),
	                  instance(),
	                  SLOT(tumblerFinishedHandler(uint)) );
	QObject::connect( _tumblerService.localData(),
	                  SIGNAL(Started(uint)),
	                  instance(),
	                  SLOT(tumblerStartedHandler(uint)) );
	QObject::connect( _tumblerService.localData(),
	                  SIGNAL(Error(uint,const QStringList&,int,const QString &)),
	                  instance(),
	                  SLOT(tumblerErrorHandler(uint,const QStringList&,int,const QString&)) );
	emit instance()->tumblerServiceReconnected ();
}

