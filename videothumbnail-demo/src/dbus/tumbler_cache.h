/*
 * This file was generated by qdbusxml2cpp version 0.7
 * Command line was: qdbusxml2cpp -c ThumbnailerGenericCacheProxy -p dbus/tumbler_cache.h:tumbler_cache.cpp -i dbus_types.h dbus/tumbler-cache-service-dbus.xml org.freedesktop.thumbnails.Cache1
 *
 * qdbusxml2cpp is Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
 *
 * This is an auto-generated file.
 * Do not edit! All changes made to it will be lost.
 */

#ifndef TUMBLER_CACHE_H_1265978319
#define TUMBLER_CACHE_H_1265978319

#include <QtCore/QObject>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>
#include <QtDBus/QtDBus>
#include "dbus_types.h"

/*
 * Proxy class for interface org.freedesktop.thumbnails.Cache1
 */
class ThumbnailerGenericCacheProxy: public QDBusAbstractInterface
{
    Q_OBJECT
public:
    static inline const char *staticInterfaceName()
    { return "org.freedesktop.thumbnails.Cache1"; }

public:
    ThumbnailerGenericCacheProxy(const QString &service, const QString &path, const QDBusConnection &connection, QObject *parent = 0);

    ~ThumbnailerGenericCacheProxy();

public Q_SLOTS: // METHODS
    inline QDBusPendingReply<> Cleanup(const QString &uri_prefix, uint since)
    {
        QList<QVariant> argumentList;
        argumentList << qVariantFromValue(uri_prefix) << qVariantFromValue(since);
        return asyncCallWithArgumentList(QLatin1String("Cleanup"), argumentList);
    }

    inline QDBusPendingReply<> Copy(const QStringList &from_uris, const QStringList &to_uris)
    {
        QList<QVariant> argumentList;
        argumentList << qVariantFromValue(from_uris) << qVariantFromValue(to_uris);
        return asyncCallWithArgumentList(QLatin1String("Copy"), argumentList);
    }

    inline QDBusPendingReply<> Delete(const QStringList &uris)
    {
        QList<QVariant> argumentList;
        argumentList << qVariantFromValue(uris);
        return asyncCallWithArgumentList(QLatin1String("Delete"), argumentList);
    }

    inline QDBusPendingReply<> Move(const QStringList &from_uris, const QStringList &to_uris)
    {
        QList<QVariant> argumentList;
        argumentList << qVariantFromValue(from_uris) << qVariantFromValue(to_uris);
        return asyncCallWithArgumentList(QLatin1String("Move"), argumentList);
    }

Q_SIGNALS: // SIGNALS
};

namespace org {
  namespace freedesktop {
    namespace thumbnails {
      typedef ::ThumbnailerGenericCacheProxy Cache1;
    }
  }
}
#endif
