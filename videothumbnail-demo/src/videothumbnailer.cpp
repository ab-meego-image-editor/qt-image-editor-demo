#include <QDir>
#include <QFile>
#include <QCryptographicHash>
#include <QDebug>
#include "dbus/thumbnailer_generic.h"
#include "dbus-services.h"
#include "videothumbnailer.h"


VideoThumbnailer::VideoThumbnailer()
{
    QObject::connect( DBusServices::instance(),
	                  SIGNAL(ReadyHandler(uint, const QStringList&)),
	                  this,
	                  SLOT(ReadyHandler(uint, const QStringList&)));
	QObject::connect( DBusServices::instance(),
	                  SIGNAL(FinishedHandler(uint)),
	                  this,
	                  SLOT(FinishedHandler(uint)));
	QObject::connect( DBusServices::instance(),
	                  SIGNAL(StartedHandler(uint)),
	                  this,
	                  SLOT(StartedHandler(uint)));
	QObject::connect( DBusServices::instance(),
	                  SIGNAL(ErrorHandler(uint,const QStringList&,int,const QString&)),
	                  this,
	                  SLOT(ErrorHandler(uint,const QStringList&,int,const QString&)));
}
void VideoThumbnailer::makeRequest(QStringList &uris,
                                   QStringList &mime,
                                   QString &flavor,
                                   QString &scheduler,
                                   uint handle)
{
    QDBusPendingReply<uint> reply;
    reply = DBusServices::tumbler()->Queue (uris,
                                    mime,
                                    flavor,
                                    scheduler,
                                    handle);
    // wait for the reply from DBus call
	QDBusPendingCallWatcher* watcher = new QDBusPendingCallWatcher( reply,
	                                                                this );
	QObject::connect(watcher,
                     SIGNAL(finished(QDBusPendingCallWatcher*)),
                     this,
                     SLOT(requestCallFinished(QDBusPendingCallWatcher*)));
}

bool VideoThumbnailer::checkExist(QString &fileName, QString &flavor)
{
    const QUrl uri =
        QUrl::fromLocalFile(QFileInfo(fileName).canonicalFilePath());
    qDebug()<<" VideoThumbnailer::the uri is: "<<uri.toString();
    const QByteArray hashValue =
        QCryptographicHash::hash(uri.toString().toLatin1(),
                                 QCryptographicHash::Md5);
    qDebug()<<"VideoThumbnailer::the hashed file name is: "<<QString(hashValue.toHex());
    if( QFile(QDir::toNativeSeparators(QDir::homePath()) +
              "/.thumbnails/" +
              flavor + QDir::separator() +
              hashValue.toHex() + ".jpeg").exists()){
        qDebug()<<"VideoThumbnailer::the existing hashed file path is: "<<QDir::toNativeSeparators(QDir::homePath()) +
            "/.thumbnails/" +flavor + QDir::separator() +hashValue.toHex() + ".jpeg";
        return true;
    }
    else
    return false;
}

void VideoThumbnailer::StartedHandler(uint handle)
{
    qDebug()<<"+++VideoThumbnailer::StartedHandler:it catches the started signal";
}

void VideoThumbnailer::ReadyHandler(uint handle, const QStringList &uris)
{
    qDebug()<<"+++VideoThumbnailer::StartedHandler:it catches the ready signal";
}

void  VideoThumbnailer::ErrorHandler( uint handle,
                                      const QStringList &failedUris,
                                      int errorCode,
                                      const QString &message )
{
    qDebug()<<"+++VideoThumbnailer::ErrorHandler:it catches the error signal";
    qDebug()<<"+++Failed Uris:"<<failedUris;
    qDebug()<<"+++Error code:"<<errorCode;
    qDebug()<<"+++Message:"<<message;
    emit error();
}

void VideoThumbnailer::FinishedHandler(uint handle)
{
    qDebug()<<"+++VideoThumbnailer::StartedHandler:it catches the finished signal";
    emit finishGenerating();
}

void VideoThumbnailer::requestCallFinished(QDBusPendingCallWatcher* watcher)
{
    qDebug()<<"+++VideoThumbnailer::requestCallFinished()";
}
VideoThumbnailer:: ~VideoThumbnailer()
{

}
