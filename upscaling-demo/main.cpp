#include <QPixmap>
#include <QLabel>
#include <QApplication>
#include <QDebug>
#include <QTime>

#include <QuillImageFilter>
#include <QuillImageFilterFactory>
#include <cmath>

#include "demo.h"

int main(int argc, char** argv)
{
    QApplication app(argc, argv);

    Demo *demo = new Demo(argv[1]);

    demo->show();
    app.exec();
    return 0;
}
