#include <QImage>
#include <QPixmap>
#include <QuillImageFilter>
#include <QuillImageFilterFactory>
#include <QDebug>
#include <QTime>
#include "demo.h"

Demo::Demo(const QString &fileName)
{
    setupUi(this);
    init(fileName);
}

void Demo::init(const QString &fileName)
{
    QuillImage inputImage = QuillImage(QImage(fileName));

    QTime time;

    time.start();
    QuillImageFilter *linear =
        QuillImageFilterFactory::createImageFilter("org.maemo.scale");
    linear->setOption(QuillImageFilter::SizeAfter, QVariant(QSize(172, 172)));
    QImage imageLinear = linear->apply(inputImage);
    qDebug() << "Time taken by Linear" << time.elapsed() << "ms";

    time.restart();
    QuillImageFilter *lanczos2 =
        QuillImageFilterFactory::createImageFilter("org.maemo.scale.lanczos");
    lanczos2->setOption(QuillImageFilter::Radius, QVariant(2));
    lanczos2->setOption(QuillImageFilter::SizeAfter, QVariant(QSize(172, 172)));
    QImage imageLanczos2 = lanczos2->apply(inputImage);
    qDebug() << "Time taken by Lanczos2" << time.elapsed() << "ms";

    time.restart();
    QuillImageFilter *lanczos3 =
        QuillImageFilterFactory::createImageFilter("org.maemo.scale.lanczos");
    lanczos3->setOption(QuillImageFilter::Radius, QVariant(3));
    lanczos3->setOption(QuillImageFilter::SizeAfter, QVariant(QSize(172, 172)));
    QImage imageLanczos3 = lanczos3->apply(inputImage);
    qDebug() << "Time taken by Lanczos3" << time.elapsed() << "ms";

    time.restart();
    QuillImageFilter *gaussian1 =
        QuillImageFilterFactory::createImageFilter("org.maemo.blur.gaussian");
    gaussian1->setOption(QuillImageFilter::Radius, QVariant(1.0));
    QImage imageLinearGaussian1 = gaussian1->apply(imageLinear);
    qDebug() << "Time taken by Gaussian1" << time.elapsed() << "ms";

    time.restart();
    QuillImageFilter *gaussian2 =
        QuillImageFilterFactory::createImageFilter("org.maemo.blur.gaussian");
    gaussian2->setOption(QuillImageFilter::Radius, QVariant(2.0));
    QImage imageLinearGaussian2 = gaussian2->apply(imageLinear);
    qDebug() << "Time taken by Gaussian2" << time.elapsed() << "ms";

    QImage imageLanczos2Gaussian1 = gaussian1->apply(imageLanczos2);
    QImage imageLanczos2Gaussian2 = gaussian2->apply(imageLanczos2);
    QImage imageLanczos3Gaussian1 = gaussian1->apply(imageLanczos3);
    QImage imageLanczos3Gaussian2 = gaussian2->apply(imageLanczos3);

    QPixmap pixmapLinear = QPixmap::fromImage(imageLinear);
    QPixmap pixmapLanczos2 = QPixmap::fromImage(imageLanczos2);
    QPixmap pixmapLanczos3 = QPixmap::fromImage(imageLanczos3);

    QPixmap pixmapLinearGaussian1 = QPixmap::fromImage(imageLinearGaussian1);
    QPixmap pixmapLanczos2Gaussian1 = QPixmap::fromImage(imageLanczos2Gaussian1);
    QPixmap pixmapLanczos3Gaussian1 = QPixmap::fromImage(imageLanczos3Gaussian1);

    QPixmap pixmapLinearGaussian2 = QPixmap::fromImage(imageLinearGaussian2);
    QPixmap pixmapLanczos2Gaussian2 = QPixmap::fromImage(imageLanczos2Gaussian2);
    QPixmap pixmapLanczos3Gaussian2 = QPixmap::fromImage(imageLanczos3Gaussian2);

    srandom(time.elapsed());

    int order[8];
    for (int i=0; i<8; i++) {
        bool unique = false;
        do {
            unique = true;
            order[i] = (random() % 800) / 100;
            for (int j=0; j<i; j++)
                if (order[j] == order[i])
                    unique = false;
        } while (!unique);
    }


    for (int i=0; i<8; i++) {
        QLabel *imageLabel;
        QLabel *scalingMethodLabel;
        QLabel *blurMethodLabel;
        switch(i) {
        case 0 :
            imageLabel = imageLabel_1;
            scalingMethodLabel = scalingMethodLabel_1;
            blurMethodLabel = blurMethodLabel_1;
            break;
        case 1 :
            imageLabel = imageLabel_2;
            scalingMethodLabel = scalingMethodLabel_2;
            blurMethodLabel = blurMethodLabel_2;
            break;
        case 2 :
            imageLabel = imageLabel_3;
            scalingMethodLabel = scalingMethodLabel_3;
            blurMethodLabel = blurMethodLabel_3;
            break;
        case 3 :
            imageLabel = imageLabel_4;
            scalingMethodLabel = scalingMethodLabel_4;
            blurMethodLabel = blurMethodLabel_4;
            break;
        case 4 :
            imageLabel = imageLabel_5;
            scalingMethodLabel = scalingMethodLabel_5;
            blurMethodLabel = blurMethodLabel_5;
            break;
        case 5 :
            imageLabel = imageLabel_6;
            scalingMethodLabel = scalingMethodLabel_6;
            blurMethodLabel = blurMethodLabel_6;
            break;
        case 6 :
            imageLabel = imageLabel_7;
            scalingMethodLabel = scalingMethodLabel_7;
            blurMethodLabel = blurMethodLabel_7;
            break;
        case 7 :
            imageLabel = imageLabel_8;
            scalingMethodLabel = scalingMethodLabel_8;
            blurMethodLabel = blurMethodLabel_8;
            break;
        default:
            return;
        }

        scalingMethodLabel->hide();
        blurMethodLabel->hide();

        switch(order[i]) {
        case 0:
            imageLabel->setPixmap(pixmapLinear);
            scalingMethodLabel->setText("Linear");
            blurMethodLabel->setText("No blur");
            break;
        case 1:
            imageLabel->setPixmap(pixmapLanczos2);
            scalingMethodLabel->setText("Lanczos-2");
            blurMethodLabel->setText("No blur");
            break;
        case 2:
            imageLabel->setPixmap(pixmapLanczos3);
            scalingMethodLabel->setText("Lanczos-3");
            blurMethodLabel->setText("No blur");
            break;
        case 3:
            imageLabel->setPixmap(pixmapLinearGaussian1);
            scalingMethodLabel->setText("Linear");
            blurMethodLabel->setText("Gaussian theta=1");
            break;
        case 4:
            imageLabel->setPixmap(pixmapLanczos2Gaussian1);
            scalingMethodLabel->setText("Lanczos-2");
            blurMethodLabel->setText("Gaussian theta=1");
            break;
        case 5:
            imageLabel->setPixmap(pixmapLanczos3Gaussian1);
            scalingMethodLabel->setText("Lanczos-3");
            blurMethodLabel->setText("Gaussian theta=1");
            break;
        case 6:
            imageLabel->setPixmap(pixmapLinearGaussian2);
            scalingMethodLabel->setText("Linear");
            blurMethodLabel->setText("Gaussian theta=2");
            break;
        case 7:
            imageLabel->setPixmap(pixmapLanczos2Gaussian2);
            scalingMethodLabel->setText("Lanczos-2");
            blurMethodLabel->setText("Gaussian theta=2");
            break;
        }
    }

    connect(revealButton, SIGNAL(clicked()), SLOT(reveal()));
}

void Demo::reveal()
{
    revealButton->hide();
    scalingMethodLabel_1->show();
    blurMethodLabel_1->show();
    scalingMethodLabel_2->show();
    blurMethodLabel_2->show();
    scalingMethodLabel_3->show();
    blurMethodLabel_3->show();
    scalingMethodLabel_4->show();
    blurMethodLabel_4->show();
    scalingMethodLabel_5->show();
    blurMethodLabel_5->show();
    scalingMethodLabel_6->show();
    blurMethodLabel_6->show();
    scalingMethodLabel_7->show();
    blurMethodLabel_7->show();
    scalingMethodLabel_8->show();
    blurMethodLabel_8->show();
}
