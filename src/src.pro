
TEMPLATE = app
TARGET = quilldemo

PKGCONFIG += quill quillmetadata

INCLUDEPATH += .


CONFIG += DEBUG
CONFIG += quill quillmetadata


# Input
HEADERS += mainWindow.h \
           imageScene.h \
           rectImage.h \
           viewscrollbar.h \
           scaledialog.h \
           rotatedialog.h \
           colorsdialog.h \
           metadata.h \
           metadatadialog.h

FORMS += menu.ui \
         scale.ui \
         rotate.ui \
         colors.ui \
         metadata.ui

SOURCES += main.cpp \
           mainWindow.cpp \
           imageScene.cpp \
           rectImage.cpp \
           viewscrollbar.cpp \
           scaledialog.cpp \
           rotatedialog.cpp \
           colorsdialog.cpp \
           metadatadialog.cpp

# --- install
headers.files = $$INSTALL_HEADERS
headers.path = $$(DESTDIR)/usr/include
target.path = $$(DESTDIR)/usr/lib
INSTALLS += target headers

# ---clean
QMAKE_CLEAN += \
	*.gcov *.gcno *.log

QMAKE_DISTCLEAN += *.gcda *.gcno *.gcov *.log *.xml coverage *.o moc_* Makefile*
