/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Alexander Bokovoy <alexander.bokovoy@nokia.com>
**
** This file is part of the Quill Demo package.
**
** Commercial Usage
** Licensees holding valid Qt Commercial licenses may use this file in
** accordance with the Qt Commercial License Agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Nokia.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain
** additional rights. These rights are described in the Nokia Qt LGPL
** Exception version 1.0, included in the file LGPL_EXCEPTION.txt in this
** package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
**
****************************************************************************/

#include <Qt>
#include <QVariant>
#include <QuillMetadata>
#include "metadatadialog.h"

MetadataDialog::MetadataDialog()
{
    setupUi(this);
}

MetadataDialog::~MetadataDialog()
{
}

void MetadataDialog::displayMetadata(const QString &fileName)
{
    QuillMetadata metadata(fileName);

    makeLabel->setText(metadata.entry(QuillMetadata::Tag_Make).toString());
    modelLabel->setText(metadata.entry(QuillMetadata::Tag_Model).toString());
    focalLengthLabel->setText(metadata.entry(QuillMetadata::Tag_FocalLength).toString());
    exposureTimeLabel->setText(metadata.entry(QuillMetadata::Tag_ExposureTime).toString());
    originalCreatedLabel->setText(metadata.entry(QuillMetadata::Tag_TimestampOriginal).toString());
    creatorLabel->setText(metadata.entry(QuillMetadata::Tag_Creator).toString());
    GPSLabel->setText(metadata.entry(QuillMetadata::Tag_GPSLatitude).toString() + " " +
                      metadata.entry(QuillMetadata::Tag_GPSLongitude).toString() + " " +
                      metadata.entry(QuillMetadata::Tag_GPSAltitude).toString());
    QString subject;
    QStringList subjects = metadata.entry(QuillMetadata::Tag_Subject).toStringList();
    foreach(QString string, subjects)
        subject = subject + string + ", ";
    if (subjects.count() >= 2)
        subject.chop(2);
    subjectLabel->setText(subject);
    cityLabel->setText(metadata.entry(QuillMetadata::Tag_City).toString());
    countryLabel->setText(metadata.entry(QuillMetadata::Tag_Country).toString());
    ratingLabel->setText(metadata.entry(QuillMetadata::Tag_Rating).toString());
    descriptionLabel->setText(metadata.entry(QuillMetadata::Tag_Description).toString());
    titleLabel->setText(metadata.entry(QuillMetadata::Tag_Title).toString());
}
