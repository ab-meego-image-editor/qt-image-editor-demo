/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Alexander Bokovoy <alexander.bokovoy@nokia.com>
**
** This file is part of the Quill Demo package.
**
** Commercial Usage
** Licensees holding valid Qt Commercial licenses may use this file in
** accordance with the Qt Commercial License Agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Nokia.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain
** additional rights. These rights are described in the Nokia Qt LGPL
** Exception version 1.0, included in the file LGPL_EXCEPTION.txt in this
** package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
**
****************************************************************************/

#ifndef IMAGE_SCENE_H
#define IMAGE_SCENE_H

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QPointF>

class QPainter;
class QGraphicsItem;
class QStyleOptionGraphicsItem;

class ImageScene :public QGraphicsScene
{
    Q_OBJECT
    public:
    enum Mode
    {
        crop = 0,
        cropInProgress,
        insertText,
        addTextBalloon,
        brushDraw,
        redEyeReduction,
        movement,
        gridView,
        editing,
        cropArea,
        changeCropArea,
        changeCropAreaDone,
        waiting
    };
    ImageScene();
    ~ImageScene();
    void addLineScene(const QPointF point1, const QPointF point2);
public slots:
    void setMode(Mode mode);
    Mode mode();
    void setFirstClick();
 signals:
    void mouseReleased();
    void mouseMoved(QPointF point, QPointF lastPoint);
    void mousePressed(QPointF point);
    void mousePressed1(QPointF point);
    //the first double click for balloon
    void mousePressed2(QPointF point);
    void cropDone(QPointF topLeft, QPointF bottomRight);
    //brush draw
    void mousePressed3();
    void mouseReleased1();
    void redEyeReduce(QPointF point);

    void mouseMovedDrawing(QPointF point1,QPointF point2);
    void transparentDrawing(QPointF point1, QPointF point2);
    void transparentLayer();
    //crop floating selection area
    void drawCropArea(QPointF point);
    void changingCropArea(QPointF point2);
    void focusChanged();
    void waitingCrop(QPointF point);
    //
    void editingModeStart(int ithImage);

protected:
    void mouseMoveEvent (QGraphicsSceneMouseEvent *mouseEvent);
    void mousePressEvent (QGraphicsSceneMouseEvent *mouseEvent);
    void mouseReleaseEvent (QGraphicsSceneMouseEvent *mouseEvent);
private slots:
    void ableDrawingStatus();
    void disableDrawingStatus();

private:
    bool drawlineFlag;
    QPointF points;
    Mode execution;
    QPointF cropStartPosition;
    bool firstClick;
    bool drawingStatus;
    QPointF cropCenterPosition;
};

#endif
