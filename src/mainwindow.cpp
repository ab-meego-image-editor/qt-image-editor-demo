/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Alexander Bokovoy <alexander.bokovoy@nokia.com>
**
** This file is part of the Quill Demo package.
**
** Commercial Usage
** Licensees holding valid Qt Commercial licenses may use this file in
** accordance with the Qt Commercial License Agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Nokia.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain
** additional rights. These rights are described in the Nokia Qt LGPL
** Exception version 1.0, included in the file LGPL_EXCEPTION.txt in this
** package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
**
****************************************************************************/

#include <QImage>
#include <QAbstractGraphicsShapeItem>
#include <QFileInfo>
#include <QDir>
#include <QFileDialog>
#include <QSize>
#include <QListIterator>
#include <QString>
#include <QDebug>
#include <QFontDialog>
#include <QColorDialog>
#include <QRadioButton>
#include <QSlider>
#include <Quill>
#include <QuillImage>
#include <QuillImageFilter>
#include <QuillImageFilterFactory>
#include <QThread>
#include <QCloseEvent>
#include <QTextCursor>
#include <cmath>
#include "mainwindow.h"
#include "ui_menu.h"
#include "rotatedialog.h"
#include "imagescene.h"
#include "rectimage.h"
#include "viewscrollbar.h"
#include "addtext.h"
#include "scaledialog.h"
#include "colorsdialog.h"
#include "metadatadialog.h"
#include <QuillImageFilterGenerator>
#include <QuillFile>
MainWindow::MainWindow():rectImage(0),rectPartialImage(0),zoomLevel(1.0),
                         hSmallMaxiValue(0),vSmallMaxiValue(0),screenRatio(0.0),
                         quillFile(0),rectBigImage(0),cropAreaWidth(100.0),cropAreaHeight(100.0),
                         cropArea(0),grabLine(0),textOk(0)

{
    setupUi(this);

    editFrame->setDisabled(true);
    menubar->hide();

    hScrollBar= graphicsView->horizontalScrollBar();
    graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    vScrollBar =graphicsView->verticalScrollBar() ;
    graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    connect(actionExit, SIGNAL(triggered()),this, SLOT(close()));
    connect(actionExit, SIGNAL(triggered()),this, SLOT(changeMode()));
    connect(actionOpen_image, SIGNAL(triggered()),this, SLOT(changeMode()));
    connect(actionSave_image, SIGNAL(triggered()),this, SLOT(save()));
    connect(actionSave_image, SIGNAL(triggered()),this, SLOT(changeMode()));
    connect(zoomInButton, SIGNAL(clicked()),this, SLOT(zoomIn()));
    connect(zoomInButton, SIGNAL(clicked()),this, SLOT(changeMode()));
    connect(zoomOutButton, SIGNAL(clicked()),this, SLOT(zoomOut()));
    connect(zoomOutButton, SIGNAL(clicked()),this, SLOT(changeMode()));
    connect(actionAuto_contrast, SIGNAL(triggered()),this, SLOT(autoContrast()));
    connect(actionFixAll, SIGNAL(triggered()),this, SLOT(autoLevels()));

    Quill::setPreviewLevelCount(3);
    Quill::setPreviewSize(0, QSize(128, 128));
    Quill::setMinimumPreviewSize(0, QSize(128, 128));
    Quill::setPreviewSize(1, QSize(128*4+10, 128*3+10));
    Quill::setPreviewSize(2, QSize((128*4+10)*2, (128*3+10)*2));

    Quill::setImageSizeLimit(QSize(10000, 10000));
    Quill::setImagePixelsLimit(24000000);
    Quill::setNonTiledImagePixelsLimit(16000000);

    Quill::setEditHistoryCacheSize(0, 50);
    Quill::setEditHistoryCacheSize(1, 10);
    Quill::setEditHistoryCacheSize(2, 2);

    Quill::setDefaultTileSize(QSize(512, 512));
    Quill::setTileCacheSize(16);

    Quill::setBackgroundRenderingColor(QColor("Blue"));
    Quill::setVectorGraphicsRenderingSize(QSize(1200, 1200));

    Quill::setThumbnailFlavorName(0, "quilldemo-grid");
    Quill::setThumbnailFlavorName(1, "screen");
    Quill::setThumbnailExtension("jpeg");

    Quill::setTemporaryFilePath(QDir::homePath() + "/.config/quill/tmp/");

    connect(undoButton, SIGNAL(clicked()), SLOT(undo()));
    connect(undoButton, SIGNAL(clicked()), SLOT(changeMode()));
    connect(redoButton, SIGNAL(clicked()), SLOT(redo()));
    connect(redoButton, SIGNAL(clicked()), SLOT(changeMode()));
    connect(revertButton, SIGNAL(clicked()), SLOT(revert()));
    connect(revertButton, SIGNAL(clicked()), SLOT(changeMode()));
    connect(restoreButton, SIGNAL(clicked()), SLOT(restore()));
    connect(restoreButton, SIGNAL(clicked()), SLOT(changeMode()));

    colorsDialog = new ColorsDialog();

    connect(colorsButton, SIGNAL(clicked()), colorsDialog, SLOT(exec()));

    connect(colorsDialog, SIGNAL(autoLevels()), SLOT(autoLevels()));
    connect(colorsDialog, SIGNAL(autoLevels()), SLOT(changeMode()));
    connect(colorsDialog, SIGNAL(autoContrast()), SLOT(autoContrast()));
    connect(colorsDialog, SIGNAL(autoContrast()), SLOT(changeMode()));

    connect(colorsDialog, SIGNAL(brightnessPlus()), SLOT(increaseBrightness()));
    connect(colorsDialog, SIGNAL(brightnessPlus()), SLOT(changeMode()));
    connect(colorsDialog, SIGNAL(brightnessMinus()), SLOT(decreaseBrightness()));
    connect(colorsDialog, SIGNAL(brightnessMinus()), SLOT(changeMode()));

    connect(colorsDialog, SIGNAL(contrastPlus()), SLOT(increaseContrast()));
    connect(colorsDialog, SIGNAL(contrastPlus()), SLOT(changeMode()));
    connect(colorsDialog, SIGNAL(contrastMinus()), SLOT(decreaseContrast()));
    connect(colorsDialog, SIGNAL(contrastMinus()), SLOT(changeMode()));

    connect(rotate90Button, SIGNAL(clicked()), SLOT(counterClockwiseRotate()));
    connect(rotate90Button, SIGNAL(clicked()), SLOT(changeMode()));
    connect(rotate270Button, SIGNAL(clicked()), SLOT(clockwiseRotate()));
    connect(rotate270Button, SIGNAL(clicked()), SLOT(changeMode()));
    connect(rotate5Button, SIGNAL(clicked()), SLOT(fastRotate()));
    connect(rotate5Button, SIGNAL(clicked()), SLOT(changeMode()));
    connect(rotateMinus5Button, SIGNAL(clicked()), SLOT(fastRotate()));
    connect(rotateMinus5Button, SIGNAL(clicked()), SLOT(changeMode()));

    connect(flipVerticalButton, SIGNAL(clicked()), SLOT(flipVertical()));
    connect(flipVerticalButton, SIGNAL(clicked()), SLOT(changeMode()));
    connect(flipHorizontalButton, SIGNAL(clicked()), SLOT(flipHorizontal()));
    connect(flipHorizontalButton, SIGNAL(clicked()), SLOT(changeMode()));

    connect(scaleButton, SIGNAL(clicked()), SLOT(scale()));
    connect(redEyeButton, SIGNAL(clicked()), SLOT(changeMode()));

    connect(cropTopButton, SIGNAL(clicked()), SLOT(fastCrop()));
    connect(cropTopButton, SIGNAL(clicked()), SLOT(changeMode()));
    connect(cropBottomButton, SIGNAL(clicked()), SLOT(fastCrop()));
    connect(cropBottomButton, SIGNAL(clicked()), SLOT(changeMode()));
    connect(cropLeftButton, SIGNAL(clicked()), SLOT(fastCrop()));
    connect(cropLeftButton, SIGNAL(clicked()), SLOT(changeMode()));
    connect(cropRightButton, SIGNAL(clicked()), SLOT(fastCrop()));
    connect(cropRightButton, SIGNAL(clicked()), SLOT(changeMode()));

    connect(metadataButton, SIGNAL(clicked()), SLOT(displayMetadata()));
    connect(metadataButton, SIGNAL(clicked()), SLOT(changeMode()));

    connect(actionBrightness_1,SIGNAL(triggered()),this, SLOT(increaseBrightness()));
    connect(actionBrightness_1, SIGNAL(triggered()),this, SLOT(changeMode()));
    connect(actionBrightness_2,SIGNAL(triggered()),this, SLOT(decreaseBrightness()));
    connect(actionBrightness_2, SIGNAL(triggered()),this, SLOT(changeMode()));
    connect(actionClockwise,SIGNAL(triggered()),this, SLOT(clockwiseRotate()));
    connect(actionClockwise, SIGNAL(triggered()),this, SLOT(changeMode()));
    connect(actionCounterClockwise,SIGNAL(triggered()),this, SLOT(counterClockwiseRotate()));
    connect(actionCounterClockwise, SIGNAL(triggered()),this, SLOT(changeMode()));
    connect(actionFlip_Vertical, SIGNAL(triggered()),this, SLOT(flipVertical()));
    connect(actionFlip_Vertical, SIGNAL(triggered()),this, SLOT(changeMode()));
    connect(actionFlip_Horizontal, SIGNAL(triggered()),this, SLOT(flipHorizontal()));
    connect(actionFlip_Horizontal, SIGNAL(triggered()),this, SLOT(changeMode()));
    connect(actionContrast,SIGNAL(triggered()),this, SLOT(increaseContrast()));
    connect(actionContrast, SIGNAL(triggered()),this, SLOT(changeMode()));
    connect(actionContrast_2,SIGNAL(triggered()),this, SLOT(decreaseContrast()));
    connect(actionContrast_2, SIGNAL(triggered()),this, SLOT(changeMode()));
    connect(actionUndo, SIGNAL(triggered()),this,SLOT(undo()));
    connect(actionUndo, SIGNAL(triggered()),this, SLOT(changeMode()));

    connect(actionRedo, SIGNAL(triggered()),this, SLOT(redo()));
    connect(actionRedo, SIGNAL(triggered()),this, SLOT(changeMode()));
    connect(actionScale, SIGNAL(triggered()),this, SLOT(scale()));
    //    connect(actionRotate_Free, SIGNAL(triggered()),this, SLOT(rotateFree()));
    connect(actionRed_eye_reduction, SIGNAL(triggered()),
            this, SLOT(changeMode()));

    /*    connect(actionRevert, SIGNAL(triggered()),this,SLOT(revert()));
    connect(actionRevert, SIGNAL(triggered()),this, SLOT(changeMode()));
    connect(actionRestore, SIGNAL(triggered()),this,SLOT(restore()));
    connect(actionRestore, SIGNAL(triggered()),this, SLOT(changeMode()));*/
    singleImageMode = false;
    imageScene = new ImageScene();
    singleImageScene = new ImageScene();
    imageScene->setSceneRect(QRect(0,0,128*4+10,128*3+10));
    singleImageScene->setSceneRect(QRect(0,0,128*4+10,128*3+10));
    graphicsView->setScene(imageScene);
    imageScene->setMode(ImageScene::gridView);
    graphicsView->setDragMode(QGraphicsView::NoDrag);
    //we calculate the aspect ration for screen
    screenRatio=graphicsView->geometry().width()*1.0/graphicsView->geometry().height()*1.0;

    connect(singleImageScene, SIGNAL(focusChanged()), SLOT(focusChanged()));

    font.setPointSize(90);
    color.setNamedColor("#ff0000");

    prevBounding = QRect();

    connect(singleImageScene, SIGNAL(redEyeReduce(QPointF)),
            this, SLOT(redEyeReduction(QPointF)));

    //grid view signals
    connect(imageScene, SIGNAL(editingModeStart(int)),
            this, SLOT(loadBigImage(int)));

    //crop
    connect(actionCrop, SIGNAL(triggered()),this, SLOT(changeMode()));
    connect(actionCrop, SIGNAL(triggered()),this, SLOT(addCropOk()));
    connect(singleImageScene, SIGNAL(drawCropArea(QPointF)), this, SLOT(drawCropFloatingArea(QPointF)));
    connect(singleImageScene, SIGNAL(changingCropArea(QPointF)), this, SLOT(changingCropFloatingArea(QPointF)));

    // fast crop and rotate
    connect(actionKeep_high_2_3, SIGNAL(triggered()), this, SLOT(fastCrop()));
    connect(actionKeep_low_2_3, SIGNAL(triggered()), this, SLOT(fastCrop()));
    connect(actionTo_right_2_3, SIGNAL(triggered()), this, SLOT(fastCrop()));
    connect(actionTo_left_2_3, SIGNAL(triggered()), this, SLOT(fastCrop()));
    connect(actionRotate_Free, SIGNAL(triggered()), this, SLOT(fastRotate()));
    connect(actionRotate_5_CCW, SIGNAL(triggered()), this, SLOT(fastRotate()));

    zoomInButton->setEnabled(false);
    zoomOutButton->setEnabled(false);

    timer.setInterval(1000);
    connect(&timer, SIGNAL(timeout()), SLOT(focusChanged()));
    timer.start();
}


MainWindow::~MainWindow()
{
    Quill::cleanup();
    //imageScene owns rectImage pointer
    delete imageScene;
    delete singleImageScene;
    delete colorsDialog;
    delete quillFile;
    quillFileList.clear();
}


void MainWindow::load(const QString &dirName)
{
    zoomLevel=1.0;
    QString dirNm = dirName;

    if (dirNm.isEmpty())
        dirNm = QFileDialog::getExistingDirectory(this, QString("Select directory to view"));
    QDir dir(dirNm);

    QStringList filters;
    filters << "*.jpeg" << "*.jpg" << "*.png" << "*.bmp" << "*.ogg"
            << "*.mp4" << "*.tif" << "*.gif" << "*.svg";
    dir.setNameFilters(filters);
    QStringList fileList = dir.entryList();

    for (int i=0; i<fileList.length(); i++)
    {
        QuillFile* file;
        if(fileList[i].endsWith(".mp4")){
            file = new QuillFile(dir.path() + "/" + fileList[i],"video/mp4");
            qDebug()<<"the file name is"<<fileList[i];
        }
        else if(fileList[i].endsWith(".svg")){
            file = new QuillFile(dir.path() + "/" + fileList[i],"image/svg+xml");
            qDebug()<<"the file name is"<<fileList[i];
        }
        else if(fileList[i].endsWith(".jpg")){
            file = new QuillFile(dir.path() + "/" + fileList[i],"image/jpeg");
            qDebug()<<"the file name is"<<fileList[i];
        }
        else if(fileList[i].endsWith(".png")){
            file = new QuillFile(dir.path() + "/" + fileList[i],"image/png");
            qDebug()<<"the file name is"<<fileList[i];
        }
        else if(fileList[i].endsWith(".tif")){
            file = new QuillFile(dir.path() + "/" + fileList[i],"image/tiff");
            qDebug()<<"the file name is"<<fileList[i];
        }
        else if(fileList[i].endsWith(".bmp")){
            file = new QuillFile(dir.path() + "/" + fileList[i],"image/bmp");
            qDebug()<<"the file name is"<<fileList[i];
        }
        else
            file = new QuillFile(dir.path() + "/" + fileList[i]);
        file->setDisplayLevel(0);
        quillFileList.append(file);
        connect(file, SIGNAL(imageAvailable(const QuillImageList)),
                this, SLOT(refresh()));
    }
}

void MainWindow::save()
{
    /*    QString fileName = QFileDialog::getSaveFileName(this, QString("Choose a file to save"), QString(""), QString("image format (*.jpg *.png)"));

          quillFile->exportFile(fileName);*/
}

void MainWindow::getFileName(const QString file)
{
    fileName = file;
    load(file);
}

void MainWindow::updateZoomLabel()
{
    int level = zoomLevel * 100;
    zoomLabel->setText(QString::number(level) + "%");
}

void MainWindow::zoomIn()
{
    qreal oldZoomLevel = zoomLevel;
    if (zoomLevel >= 50)
        zoomLevel = zoomLevel;
    else if (zoomLevel >= 25)
        zoomLevel += 2.5;
    else if (zoomLevel >= 15)
        zoomLevel += 2.0;
    else if (zoomLevel >= 6)
        zoomLevel += 1.0;
    else if (zoomLevel >= 3)
        zoomLevel += .75;
    else if (zoomLevel >= 2)
        zoomLevel += .5;
    else
        zoomLevel += .25;
    updateZoomLabel();
    graphicsView->scale(zoomLevel/oldZoomLevel,zoomLevel/oldZoomLevel);
    graphicsView->show();
    hSmallMaxiValue=hSmallMaxiValue-hBigMaxiValue+hScrollBar->maximum();
    vSmallMaxiValue=vSmallMaxiValue-vBigMaxiValue+vScrollBar->maximum();
    hBigMaxiValue=hScrollBar->maximum();
    vBigMaxiValue=vScrollBar->maximum();

    focusChanged();
}

void MainWindow::zoomOut()
{
    if (zoomLevel <= 1)
    {
        returnGridView();
    }
    else
    {
        qreal oldZoomLevel = zoomLevel;
        if (zoomLevel > 25)
            zoomLevel -= 2.5;
        else if (zoomLevel > 15)
            zoomLevel -= 2.0;
        else if (zoomLevel > 6)
              zoomLevel -= 1.0;
        else if (zoomLevel > 3)
            zoomLevel -= .75;
        else if (zoomLevel > 2)
            zoomLevel -= .5;
        else
            zoomLevel -= .25;

        graphicsView->scale(zoomLevel/oldZoomLevel,zoomLevel/oldZoomLevel);
        graphicsView->show();
        updateZoomLabel();

        hSmallMaxiValue=hSmallMaxiValue-hBigMaxiValue+hScrollBar->maximum();
        vSmallMaxiValue=vSmallMaxiValue-vBigMaxiValue+vScrollBar->maximum();
        hBigMaxiValue=hScrollBar->maximum();
        vBigMaxiValue=vScrollBar->maximum();
    }

    focusChanged();
}

void MainWindow::refresh()
{
    if (singleImageMode)
    {
        refreshSingleImageView();
        return;
    }

    while(imageScene->items().count()!=0){
        imageScene->removeItem(imageScene->items().last());
    }
    rectImageList.clear();
    for(int i =0; i<quillFileList.count();i++){
        QList<QuillImage> levels = quillFileList.at(i)->allImageLevels();

        if (levels.isEmpty())
            continue;
        QuillImage image = levels.last();

        hSmallValue=hScrollBar->sliderPosition();
        vSmallValue=vScrollBar->sliderPosition();
        hSmallMaxiValue=hScrollBar->maximum();
        vSmallMaxiValue=vScrollBar->maximum();
        rectImage = new RectImage(image);
        rectImage->setPos(128*fmod(i,4),floor(i/4)*128);
        rectImageList.append(rectImage);
        hScrollBar->setSliderPosition(hSmallValue);
        vScrollBar->setSliderPosition(vSmallValue);

        hBigMaxiValue=hScrollBar->maximum();
        vBigMaxiValue=vScrollBar->maximum();
    }
    for(int i=0;i<rectImageList.count();i++)
        imageScene->addItem(rectImageList.at(i));
    graphicsView->resetMatrix();
    graphicsView->show();
}

void MainWindow::refreshSingleImageView()
{
    if (!quillFile)
        return;
    //testing to make a core
    QList<QuillImage> levels = quillFile->allImageLevels();

    //qDebug() << "image z-levels follow";
    foreach (QuillImage image, levels) {
        //qDebug() << image.z();
    }

    if (levels.isEmpty())
        return;
    QuillImage image = levels.last();
    if (image.isFragment())
    {
        refreshPartial();
        return;
    }

    hSmallValue=hScrollBar->sliderPosition();
    vSmallValue=vScrollBar->sliderPosition();
    hSmallMaxiValue=hScrollBar->maximum();
    vSmallMaxiValue=vScrollBar->maximum();
    if (rectBigImage) {
        singleImageScene->removeItem(rectBigImage);
        delete rectBigImage;
    }
    if (rectPartialImage) {
        singleImageScene->removeItem(rectPartialImage);
        prevBounding = QRect();
        delete rectPartialImage;
        rectPartialImage = 0;
    }
    rectBigImage = new RectImage(image);
    singleImageScene->addItem(rectBigImage);
    singleImageScene->setSceneRect(QRect(QPoint(0, 0), image.size()));

    scaleGraphicsView();
    hScrollBar->setSliderPosition(hSmallValue);
    vScrollBar->setSliderPosition(vSmallValue);
    hBigMaxiValue=hScrollBar->maximum();
    vBigMaxiValue=vScrollBar->maximum();
    graphicsView->show();
}

void MainWindow::refreshPartial()
{
    hSmallValue=hScrollBar->sliderPosition();
    vSmallValue=vScrollBar->sliderPosition();
    hSmallMaxiValue=hScrollBar->maximum();
    vSmallMaxiValue=vScrollBar->maximum();

    if (rectBigImage && (rectBigImage->getImage().size() != quillFile->fullImageSize()))
    {
        singleImageScene->removeItem(rectBigImage);
        delete rectBigImage;
        rectBigImage = new RectImage(quillFile->image());

        rectBigImage->scale(quillFile->fullImageSize().width() * 1.0 /
                            quillFile->image().width(),
                            quillFile->fullImageSize().height() * 1.0 /
                            quillFile->image().height());
        singleImageScene->addItem(rectBigImage);
    }

    QList<QuillImage> tiles = quillFile->allImageLevels();

    QRect bounding;
    for (int i=0; i<tiles.count(); i++)
        if (tiles[i].isFragment()) {
            if (bounding.isEmpty())
                bounding = tiles[i].area();
            else
                bounding = bounding.united(tiles[i].area());
        }
    if (bounding != prevBounding) {
        tileImage = QImage(bounding.size(), QImage::Format_ARGB32);
        tileImage.fill(qRgba(0, 0, 0, 0));

        QPainter painter(&tileImage);

        for (int i=0; i<tiles.count(); i++) {
            if (tiles[i].isFragment()){
                painter.drawImage(tiles[i].area().topLeft() - bounding.topLeft(),
                                  tiles[i]);
            }
        }

        painter.end();

        if (rectPartialImage)
            singleImageScene->removeItem(rectPartialImage);
        delete rectPartialImage;

        rectPartialImage = new RectImage(tileImage);
        rectPartialImage->setPos(bounding.topLeft());

        rectPartialImage->setZValue(1);
        rectBigImage->setZValue(0);

        singleImageScene->setSceneRect(QRect(QPoint(0,0),
                                             quillFile->fullImageSize()));

        singleImageScene->addItem(rectPartialImage);

        scaleGraphicsView();

        prevBounding = bounding;
    }
    else
        if(!tileImage.isNull())
            rectPartialImage->setImage(tileImage);

    hScrollBar->setSliderPosition(hSmallValue);
    vScrollBar->setSliderPosition(vSmallValue);
    hBigMaxiValue=hScrollBar->maximum();
    vBigMaxiValue=vScrollBar->maximum();

    graphicsView->show();
}

QRect MainWindow::viewPort()
{
    QRectF rect = graphicsView->mapToScene(graphicsView->rect()).boundingRect();

    qreal xScale = 1.0 * quillFile->fullImageSize().width() /
        singleImageScene->sceneRect().width();
    qreal yScale = 1.0 * quillFile->fullImageSize().height() /
        singleImageScene->sceneRect().height();

    return QRect(rect.left() * xScale, rect.top() * yScale,
                 rect.width() * xScale, rect.height() * yScale);
}

void MainWindow::increaseBrightness()
{
    QuillImageFilter *filter =
        QuillImageFilterFactory::createImageFilter("org.maemo.composite.brightness.contrast");

    filter->setOption(QuillImageFilter::Brightness, QVariant(10));

    quillFile->runFilter(filter);
}

void MainWindow::decreaseBrightness()
{
    QuillImageFilter *filter =
        QuillImageFilterFactory::createImageFilter("org.maemo.composite.brightness.contrast");

    filter->setOption(QuillImageFilter::Brightness, QVariant(-10));

    quillFile->runFilter(filter);
}

void MainWindow::recover()
{
    // removed
}

void MainWindow::undo()
{
    quillFile->undo();
}

void MainWindow::redo()
{
    quillFile->redo();
}

void MainWindow::increaseContrast()
{
    QuillImageFilter *filter =
        QuillImageFilterFactory::createImageFilter("org.maemo.composite.brightness.contrast");
    filter->setOption(QuillImageFilter::Contrast, 20);

    quillFile->runFilter(filter);
}

void MainWindow::decreaseContrast()
{
    QuillImageFilter *filter =
        QuillImageFilterFactory::createImageFilter("org.maemo.composite.brightness.contrast");
    filter->setOption(QuillImageFilter::Contrast, -20);

    quillFile->runFilter(filter);
}

void MainWindow::autoContrast()
{
    QuillImageFilter *autoContrast =
        QuillImageFilterFactory::createImageFilter("org.maemo.auto.contrast");
    quillFile->runFilter(autoContrast);
}

void MainWindow::autoLevels()
{
    QuillImageFilter *autoLevels =
        QuillImageFilterFactory::createImageFilter("org.maemo.auto.levels");

    quillFile->runFilter(autoLevels);
}


void MainWindow::storeCacheLimit(const QString resolution, const QString cost){
    int level = resolution.toInt();
    int limit = cost.toInt();
    Quill::setEditHistoryCacheSize(level, limit);
}

void MainWindow::counterClockwiseRotate()
{
    QuillImageFilter *filter =
        QuillImageFilterFactory::createImageFilter("org.maemo.rotate");

    filter->setOption(QuillImageFilter::Angle, QVariant(-90));

    quillFile->runFilter(filter);
}

void MainWindow::clockwiseRotate()
{
    QuillImageFilter *filter =
        QuillImageFilterFactory::createImageFilter("org.maemo.rotate");
    quillFile->runFilter(filter);
}

void MainWindow::flipHorizontal()
{
    QuillImageFilter *filter =
        QuillImageFilterFactory::createImageFilter("org.maemo.flip");
    filter->setOption(QuillImageFilter::Horizontal, QVariant(true));
    quillFile->runFilter(filter);
}


void MainWindow::flipVertical()
{
    QuillImageFilter *filter =
        QuillImageFilterFactory::createImageFilter("org.maemo.flip");

    quillFile->runFilter(filter);
}


void MainWindow::scaleGraphicsView()
{
    QSize size;

    if (rectPartialImage != 0)
        size = quillFile->fullImageSize();
    else if (singleImageMode)
        size = rectBigImage->getImage().size();
    else
        size = rectImage->getImage().size();

    //qDebug() << quillFile->fullImageSize();

    float widthRatio = 1.0, heightRatio = 1.0;

    imageSize= quillFile->fullImageSize();

    // For thumbnail-only supported formats, there is no valid image size
    if (imageSize.isValid()) {
        imageRatio=imageSize.width()*1.0/imageSize.height()*1.0;

        if(screenRatio>imageRatio)
            widthRatio = imageRatio / screenRatio;
        else
            heightRatio = screenRatio / imageRatio;
    }

    QMatrix matrix;

    matrix.scale((graphicsView->geometry().width()*1.0/size.width())*zoomLevel*widthRatio,
                 graphicsView->geometry().height()*1.0/size.height()*zoomLevel*heightRatio);

    graphicsView->setMatrix(matrix);
}

void MainWindow::changeMode()
{
    if (!singleImageMode)
        return;

    else if(sender() == actionCrop){
        graphicsView->setDragMode(QGraphicsView::ScrollHandDrag);
        singleImageScene->setMode(ImageScene::crop);
        cropAreaWidth = 100.0;
        cropAreaHeight = 100.0;
    }
    else if ((sender() == actionRed_eye_reduction) ||
             (sender() == redEyeButton)) {
        singleImageScene->setMode(ImageScene::redEyeReduction);
        graphicsView->setDragMode(QGraphicsView::QGraphicsView::ScrollHandDrag);
    }
    else if(sender() == actionOpen_image){
        singleImageScene->setMode(ImageScene::gridView);
        graphicsView->setDragMode(QGraphicsView::QGraphicsView::NoDrag);
    }
    else {
        singleImageScene->setMode(ImageScene::movement);
        graphicsView->setDragMode(QGraphicsView::ScrollHandDrag);
    }
}

void MainWindow::scale()
{
    ScaleDialog scaleDialog;
    QSize size = quillFile->fullImageSize();
    scaleDialog.setMax(size);

    if (!scaleDialog.exec())
        return;

    size = QSize(scaleDialog.widthSlider->value(),
                 scaleDialog.heightSlider->value());
    QuillImageFilter *filter =
        QuillImageFilterFactory::createImageFilter("org.maemo.scale");
    filter->setOption(QuillImageFilter::SizeAfter, size);

    quillFile->runFilter(filter);

}

void MainWindow::rotateFree()
{
    RotateDialog rotateDialog;

    rotateDialog.angleEdit->setText(QString::number(0));
    rotateDialog.exec();

    double angle = rotateDialog.angleEdit->text().toDouble();

    QuillImageFilter *filter =
        QuillImageFilterFactory::createImageFilter("org.maemo.free-rotate");
    filter->setOption(QuillImageFilter::Angle, QVariant(angle));
    quillFile->runFilter(filter);
}

QPointF MainWindow::transformSceneToFullImage(QPointF scene)
{
    return QPointF(scene.x() * quillFile->fullImageSize().width() /
                   singleImageScene->width(),
                   scene.y() * quillFile->fullImageSize().height() /
                   singleImageScene->height());
}

void MainWindow::redEyeReduction(QPointF point)
{
    QuillImageFilter *redEyeDetection =
        QuillImageFilterFactory::createImageFilter("org.maemo.red-eye-detection");

    redEyeDetection->setOption(QuillImageFilter::Center, QVariant(transformSceneToFullImage(point).toPoint()));

    int radius = 150;
    if (quillFile->fullImageSize().width() < 1500)
        radius = quillFile->fullImageSize().width() / 10;
    if (quillFile->fullImageSize().height() / 10 < radius)
        radius = quillFile->fullImageSize().height() / 10;

    redEyeDetection->setOption(QuillImageFilter::Radius, QVariant(radius));
    quillFile->runFilter(redEyeDetection);
}

void MainWindow::crop(QPointF center)
{
    QPoint topLeftI = QPoint(center.x()-cropAreaWidth/2, center.y()-cropAreaHeight/2);
    QPoint bottomRightI = QPoint(center.x()+cropAreaWidth/2, center.y()+cropAreaHeight/2);

    QRect cropRect(topLeftI, bottomRightI);
    cropRect = cropRect.normalized();

    QuillImageFilter *filter =
        QuillImageFilterFactory::createImageFilter("org.maemo.crop");
    filter->setOption(QuillImageFilter::CropRectangle, QVariant(cropRect));
    quillFile->runFilter(filter);
}

void MainWindow::focusChanged()
{
    if (!singleImageMode)
        return;

    if (!quillFile)
        return;

    if (zoomLevel > 2)
        quillFile->setViewPort(viewPort());
    else
        quillFile->setViewPort(QRect());
}

void MainWindow::loadBigImage(int ithImage)
{
    editFrame->setDisabled(false);
    if (ithImage >= quillFileList.count())
        return;

    singleImageMode = true;
    zoomLevel = 1.0;
    updateZoomLabel();
    zoomInButton->setEnabled(true);
    zoomOutButton->setEnabled(true);

    delete quillFile;

    fileName = quillFileList.at(ithImage)->fileName();

    setWindowTitle("QuillDemo [" + fileName + "]");

    quillFile = new QuillFile(fileName);
    quillFile->setDisplayLevel(3);
    connect(quillFile, SIGNAL(imageAvailable(const QuillImageList)),
            this, SLOT(refresh()));

    graphicsView->setScene(singleImageScene);
    singleImageScene->setMode(ImageScene::movement);
    refreshSingleImageView();
}

void MainWindow::returnGridView()
{
    editFrame->setDisabled(true);
    setWindowTitle("QuillDemo [thumbnail layout]");

    quillFile->save();

    singleImageMode = false;
    zoomLevel = 1.0;
    updateZoomLabel();
    zoomInButton->setEnabled(false);
    zoomOutButton->setEnabled(false);

    graphicsView->setScene(imageScene);
    imageScene->setSceneRect(QRect(0,0,128*4+10,128*3+10));
    /*    for(int i = 0;i<quillFileList.count();i++)
          quillFileList.at(i)->setDisplayLevel(0);*/
    if(rectBigImage){
        imageScene->removeItem(rectBigImage);
        delete rectBigImage;
        rectBigImage = 0;
    }
    graphicsView->setDragMode(QGraphicsView::NoDrag);
    refresh();
}

void MainWindow::drawCropFloatingArea(QPointF point)
{
    //We restrict the selection area is within the scene
    if((point.x()-cropAreaWidth/2>0&&point.x()+cropAreaWidth/2<640)
       &&(point.y()-cropAreaHeight/2>0&&point.y()+cropAreaHeight/2<480)){
        //we catch the grab area
        QPen bgPen;
        bgPen.setWidth(3);
        bgPen.setColor(Qt::white);

        QPen pen(Qt::DotLine);
        pen.setWidth(3);
        pen.setColor(Qt::black);

        if(cropArea){
            singleImageScene->removeItem(cropArea);
            singleImageScene->removeItem(bgCropArea);
            delete cropArea;
        }
        if(grabLine){
            singleImageScene->removeItem(grabLine);
            singleImageScene->removeItem(bgGrabLine);
            delete grabLine;
        }
        if(QRectF(QPointF(cropRectBottomRight.x()-30,cropRectBottomRight.y()-30),QSizeF(30,30)).contains(point)){
            singleImageScene->setMode(ImageScene::changeCropArea);
            QRectF cropRect(cropRectCenter.x()-cropAreaWidth/2, cropRectCenter.y()-cropAreaHeight/2,
                            cropAreaWidth, cropAreaHeight);
            bgCropArea = singleImageScene->addRect(cropRect, bgPen);
            cropArea = singleImageScene->addRect(cropRect, pen);
            int diff = 30;
            bgGrabLine = singleImageScene->addLine(cropRect.bottomRight().x()-diff,cropRect.bottomRight().y(),
                                           cropRect.bottomRight().x(),cropRect.bottomRight().y()-diff, bgPen);
            grabLine = singleImageScene->addLine(cropRect.bottomRight().x()-diff,cropRect.bottomRight().y(),
                                           cropRect.bottomRight().x(),cropRect.bottomRight().y()-diff,pen);
            graphicsView->show();
            previousPoint= point;
        }
        else{
            QRectF cropRect(point.x()-cropAreaWidth/2, point.y()-cropAreaHeight/2,
                            cropAreaWidth, cropAreaHeight);
            cropRectBottomRight = cropRect.bottomRight();
            cropRectCenter = cropRect.center();
            bgCropArea = singleImageScene->addRect(cropRect, bgPen);
            cropArea = singleImageScene->addRect(cropRect, pen);
            int diff = 30;
            bgGrabLine = singleImageScene->addLine(cropRect.bottomRight().x()-diff,cropRect.bottomRight().y(),
                                           cropRect.bottomRight().x(),cropRect.bottomRight().y()-diff,bgPen);
            grabLine = singleImageScene->addLine(cropRect.bottomRight().x()-diff,cropRect.bottomRight().y(),
                                           cropRect.bottomRight().x(),cropRect.bottomRight().y()-diff,pen);
            graphicsView->show();
        }
    }
}

void MainWindow::changingCropFloatingArea(QPointF point2)
{
    if((point2.x()>0&&point2.x()<640)&&(point2.y()>0&&point2.y()<480)){
        //We restrict the selection area is within the scene
        double hDistance = fabs(point2.x()-cropRectCenter.x());
        double vDistance = fabs(point2.y()-cropRectCenter.y());
        if((cropRectCenter.x()+hDistance<640&&cropRectCenter.x()-hDistance>0)
           &&(cropRectCenter.y()-vDistance>0&&cropRectCenter.y()+vDistance<480)){
            //we need to calculate the selection area here
            QPen bgPen;
            bgPen.setWidth(3);
            bgPen.setColor(Qt::white);

            QPen pen(Qt::DotLine);
            pen.setWidth(3);
            pen.setColor(Qt::black);
            if(cropArea){
                singleImageScene->removeItem(cropArea);
                singleImageScene->removeItem(bgCropArea);
                delete cropArea;
                delete bgCropArea;
            }
            if(grabLine){
                singleImageScene->removeItem(grabLine);
                singleImageScene->removeItem(bgGrabLine);
                delete grabLine;
                delete bgGrabLine;
            }

            cropAreaWidth = hDistance*2;
            cropAreaHeight = vDistance*2;
            QRectF cropRect(cropRectCenter.x()-cropAreaWidth/2, cropRectCenter.y()-cropAreaHeight/2,
                            cropAreaWidth, cropAreaHeight);
            bgCropArea = singleImageScene->addRect(cropRect, bgPen);
            cropArea = singleImageScene->addRect(cropRect, pen);
            int diff = 30;
            bgGrabLine = singleImageScene->addLine(cropRect.bottomRight().x()-diff,cropRect.bottomRight().y(),
                                           cropRect.bottomRight().x(),cropRect.bottomRight().y()-diff, bgPen);
            grabLine = singleImageScene->addLine(cropRect.bottomRight().x()-diff,cropRect.bottomRight().y(),
                                           cropRect.bottomRight().x(),cropRect.bottomRight().y()-diff,pen);
            graphicsView->show();
            previousPoint = point2;
        }
    }
    else if((point2.x()>-160&&point2.x()<-30)&&(point2.y()>40&&point2.y()<130)){
        if(textOk){
            singleImageScene->removeItem(textOk);
            delete textOk;
        }
        crop(cropRectCenter);
    }
}

void MainWindow::addCropOk()
{
    if(textOk){
        singleImageScene->removeItem(textOk);
        delete textOk;
    }
    QFont okFont;
    okFont.setBold(true);
    okFont.setPointSize(50);
    QString ok("OK");
    textOk = singleImageScene->addText(ok,okFont);
    textOk->setPos(-160,50);
    graphicsView->show();
}

void MainWindow::fastCrop()
{
    QuillImageFilter *filter =
        QuillImageFilterFactory::createImageFilter("org.maemo.crop");

    QRect cropRect = QRect(QPoint(0, 0), quillFile->fullImageSize());

    if ((sender() == actionKeep_high_2_3) || (sender() == cropTopButton))
        cropRect.setBottom((cropRect.top() + cropRect.bottom() * 2) / 3);

    else if ((sender() == actionKeep_low_2_3) || (sender() == cropBottomButton))
        cropRect.setTop((cropRect.top() * 2 + cropRect.bottom()) / 3);

    else if ((sender() == actionTo_right_2_3) || (sender() == cropRightButton))
        cropRect.setLeft((cropRect.left() * 2 + cropRect.right()) / 3);

    else if ((sender() == actionTo_left_2_3) || (sender() == cropLeftButton))
        cropRect.setRight((cropRect.left() + cropRect.right() * 2) / 3);

    filter->setOption(QuillImageFilter::CropRectangle, QVariant(cropRect));

    quillFile->runFilter(filter);
}

void MainWindow::fastRotate()
{
    QuillImageFilter *filter =
        QuillImageFilterFactory::createImageFilter("org.maemo.free-rotate");

    float angle;

    if ((sender() == actionRotate_Free) || (sender() == rotate5Button))
        angle = 5.0;
    else
        angle = -5.0;

    filter->setOption(QuillImageFilter::Angle, QVariant(angle));

    quillFile->runFilter(filter);
}

void MainWindow::revert()
{
    quillFile->revert();
}
void MainWindow::restore()
{
    quillFile->restore();
}

void MainWindow::displayMetadata()
{
    MetadataDialog metadataDialog;
    metadataDialog.displayMetadata(quillFile->fileName());
    metadataDialog.exec();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    Quill::waitUntilFinished();
    event->accept();
}
